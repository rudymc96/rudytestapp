## Frontend en React js

* Paso 1
    * Instalamos NodeJs con los siguientes comandos

        > `curl -sL https://deb.nodesource.com/setup_15.x | sudo -E bash -`
        
        > `sudo apt-get install -y nodejs`
        
* Paso 2
    * Clonamos el repositorio en la carpeta projectos o donde guste con el siguiente comando
    
        > `git clone https://@bitbucket.org/rudymc96/rudytestapp.git`
        
* Paso 3
    * Instalamos las dependencias del proyecto con el siguiente comando

        > `npm install`
        
* Paso 4
    * Luego ejecutamos el proyecto con el siguiente comando
    
        > `npm start`

* Paso 5
    * Para solucionar el problema de los cors instalamos un complemento en chrome llamado "Allow CORS: Access-Control-Allow-origin" que solucionara el problema de los permisos para realizar peticiones a laravel
       