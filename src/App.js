import { Button, makeStyles } from '@material-ui/core';
import { useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";

import './App.css';
import Problema1 from './components/Problema1';
import Problema2 from './components/Problema2';

const useStyles = makeStyles((theme) => ({
    containerApp: {
      padding: theme.spacing(3)
    },
    button: {
      margin: theme.spacing(1)
    }
}));

function App() {
  const classes = useStyles();

  const [problem, setProblem] = useState(true);

  const changeProblem = () => {
    setProblem(!problem);
  }

  return (
    <Router>
      <div className={classes.containerApp} >
        <div >
          <Button variant="contained" color="primary" component={Link} onClick={changeProblem} to="/problema1" disabled={problem}
                  className={classes.button} >
            Problema 1
          </Button>
          <Button variant="contained" color="primary" component={Link} onClick={changeProblem} to="/problema2" disabled={!problem}
                  className={classes.button} >
            Problema 2
          </Button>
        </div>
        <Switch>
          <Route exact path="/" >
            <Redirect to="/problema1" ></Redirect>
          </Route>
          <Route path="/problema1">
            <Problema1></Problema1>
          </Route>
          <Route path="/problema2">
            <Problema2></Problema2>
          </Route>
          
        </Switch>
      </div>
    </Router>
  );
}

export default App;
