import React, { useState } from 'react';
import Axios from 'axios';
import { Button, TextField, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    container: {
      padding: theme.spacing(1)
    }
}));

function Problema1() {
    const classes = useStyles();

    const [input, setInput] = useState('');
    const [output, setOutput] = useState('');

    const calculate = () => {
        Axios.get('http://apitestrudy.test/api/problem1', {
            params: {
                input: input
            }})
      .then(res => {
        setOutput(res.data.output);
      })
    }

    const onChangeInput = (event) => {
        setInput(event.target.value);
    };

    return (
        <div className={classes.container} >
            <Typography variant="h4" component="h4" className={classes.container} >
                Problema 1
            </Typography>
            <div>
                <TextField
                    id="input-1"
                    label="Problema 1"
                    multiline
                    rows={10}
                    value={input}
                    onChange={onChangeInput}
                    variant="outlined"
                />
                <pre id="output-1">{output}</pre>

                <Button variant="contained" color="primary" onClick={calculate}>
                    Calcular
                </Button>
            </div>
        </div>
    )
}

export default Problema1;