import { Button, makeStyles, TextField, Typography } from '@material-ui/core';
import Axios from 'axios';
import React, { useState } from 'react'

const useStyles = makeStyles((theme) => ({
    container: {
      padding: theme.spacing(1)
    }
}));

function Problema2() {
    const classes = useStyles();

    const [input, setInput] = useState('');
    const [output, setOutput] = useState('');

    const onChangeInput = (event) => {
        setInput(event.target.value);
    };

    const calculate = () => {
        Axios.get('http://apitestrudy.test/api/problem2', {
            params: {
                input: input
            }})
      .then(res => {
        setOutput(res.data.output);
      })
    }
    
    return (
        <div className={classes.container} >
            <Typography variant="h4" component="h4" className={classes.container} >
                Problema 2
            </Typography>
            <div >
                <TextField
                    id="input-1"
                    label="Problema 2"
                    multiline
                    rows={10}
                    value={input}
                    onChange={onChangeInput}
                    variant="outlined"
                />
                <pre id="output-1">{output}</pre>

                <Button variant="contained" color="primary" onClick={calculate}>
                    Analizar
                </Button>
            </div>
        </div>
    )
}

export default Problema2;